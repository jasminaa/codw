import math


def nearest_sq(n):
    root = math.sqrt(n)
    round_root = round(root)
    return int(round_root) ** 2
