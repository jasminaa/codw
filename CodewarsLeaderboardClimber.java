public class CodewarsLeaderboardClimber {
    public static String leaderBoard(String user, int userScore, int yourScore) {
        int dif = (userScore - yourScore) + 1;
        int kyu = dif % 3;
        int beta = dif - kyu;
        int total = (dif / 3) + kyu;
        String output = "To beat " + user + "'s score, I must complete " + beta / 3 + " Beta kata and " + kyu + " 8kyu kata.";

        if (yourScore > userScore) {
            return "Winning!";
        }
        if (yourScore == userScore) {
            return "Only need one!";
        }
        if (total < 1000) {
            return output;
        }
        return output + " Dammit!";
    }
}