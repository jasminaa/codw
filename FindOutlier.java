import java.lang.Math;


public class FindOutlier {
    static int find(int[] integers) {
        int sumOfThree = Math.abs(integers[0] % 2) + Math.abs(integers[1] % 2) + Math.abs(integers[2] % 2);
        for (int i : integers) {
            if (sumOfThree > 1 && i % 2 == 0) {
                return i;
            }
            if (sumOfThree <= 1 && i % 2 != 0) {
                return i;
            }
        }
        return 0;
    }
}