def find_outlier(integers):
    sum = (integers[0] % 2) + (integers[1] % 2) + (integers[2] % 2)
    for i in integers:
        if sum > 1 and i % 2 == 0:
            return i
        if sum <= 1 and i % 2 != 0:
            return i
