import java.lang.Math;


public class CodeWarsMath {
    public static int nearestSq(final int n){

        double squareRoot = Math.sqrt(n);
        double roundRoot = Math.round(squareRoot);
        int square = (int)Math.pow(roundRoot, 2);

        return square;
    }
}