def plural(n):
    if n > 1:
        return "s"
    return ""


def format_duration(seconds):
    form = ["year", "day", "hour", "minute", "second"]
    year = seconds / (365*24*60*60)
    day = (seconds % (365*24*60*60)) / (24*60*60)
    hour = (seconds % (24*60*60)) / (60*60)
    minute = (seconds % (60*60)) / 60
    second = seconds % 60
    num = [year, day, hour, minute, second]

    if seconds == 0:
        return "now"

    final = [str(i) + " " + x + plural(i) for i, x in zip(num, form) if i != 0]
    if len(final) != 1:
        return ", ".join(final[:len(final) - 1]) + " and " + final[len(final) - 1]
    return "".join(final)
