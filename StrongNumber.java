import java.util.Arrays;
import java.util.List;


class StrongNumber {

    public static String isStrongNumber(int num) {
        int sumOfNum = 0;
        String[] numString = Integer.toString(num).split("");
        List<String> numList = Arrays.asList(numString);
        for (int i = 0; i < numList.size(); i++) {
            int numInList = Integer.parseInt(numList.get(i));
            sumOfNum += factorial(numInList);
        }
        if (sumOfNum == num)
            return "STRONG!!!!";
        return "Not Strong !!";
    }

    public static int factorial(int number) {
        int fact = 1;
        for (int i = 1; i <= number; i++) {
            fact *= i;
        }
        return fact;
    }

}