import java.util.*;


public class TimeFormatter {

    public static String formatDuration(int seconds) {
        List<String> units = new ArrayList<>(Arrays.asList("year", "day", "hour", "minute", "second"));
        int year = seconds / (365*24*60*60);
        int day = (seconds % (365*24*60*60)) / (24*60*60);
        int hour = (seconds % (24*60*60)) / (60*60);
        int minute = (seconds % (60*60)) / 60;
        int second = seconds % 60;
        List<Integer> values = new ArrayList<>(Arrays.asList(year, day, hour, minute, second));

        if (seconds == 0) {
            return "now";
        }

        List<String> strArr = new ArrayList<>();
        for (int i = 0; i < values.size(); ++i) {
            int count = values.get(i);
            if (count != 0) {
                String str = plural(count, units.get(i));
                strArr.add(str);
            }
        }

        int lastIndex = strArr.size() - 1;
        String lastStr = strArr.get(lastIndex);

        if (lastIndex == 0) {
            return lastStr;
        }

        strArr.remove(lastIndex);
        String s = String.join(", ", strArr);
        return s + " and " + lastStr;

    }

    private static String plural(int n, String format) {
        if (n == 1) {
            return "" + n + " " + format;
        }
        return "" + n + " " + format + "s";
    }

}